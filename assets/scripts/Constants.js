module.exports = {
    enum_ui: {
        0:0
        ,ui_analytics_TitleEvent                : 1
        ,ui_analytics_PlayerEvent               : 2
        ,ui_analytics_CharacterEvent            : 3

    }, // enum_ui


    ui_keys: {
        0:0
        // analytic_attribute_form
        ,analytic_attribute_form_adid : "analytic_attribute_form_adid"
        ,analytic_attribute_form_idfa : "analytic_attribute_form_idfa"

        // analytic_device_form
        ,analytic_device_form_info : "analytic_device_form_info"

        // analytic_data_form
        ,analytic_data_form_identifier : "analytic_data_form_identifier"
        ,analytic_data_form_timestamp : "analytic_data_form_timestamp"
        ,analytic_data_form_body : "analytic_data_form_body"
    }, // ui_keys
  };
  