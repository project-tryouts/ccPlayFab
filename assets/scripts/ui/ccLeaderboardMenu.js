
// manager
const pfLeaderboard = require("./playfab/pfLeaderboard");

cc.Class({
    extends: cc.Component,

    properties: {
        identifier_editbox : {
            type: cc.EditBox,
            default: null,
        }, // identifier_editbox

        score_editbox : {
            type: cc.EditBox,
            default: null,
        }, // score_editbox


        // the leaderboard scroll view container
        leaderboard_scrollview: {
            type: cc.ScrollView,
            default: null,
        }, // leaderboard_scrollview

        // the template
        item_template: {
            type: cc.Label,
            default: null,
        }, // item_template
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        console.log( Global.Account );

        let contents = [
            { PlayFabId: "test", StatValue: 0},
        ];
        this.updateScrollView(contents);
    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////////
    updateScrollView :function (contents) {
        cc.assert( null != this.leaderboard_scrollview, "null leaderboard scrollview" );
        cc.assert( null != this.item_template, "null item template" );
        cc.assert( null != this.item_template.node, "null item template node" );

        let container = this.leaderboard_scrollview.content;

        // cleanup all the old content
        container.destroyAllChildren();

        for( let i = 0 ; i < contents.length; ++i )
        {
            let content = contents[i];
            let text = "";
            /////////////////////////////////////////////
            // content.PlayFabId;
            // content.Position;
            // content.StatValue;
            
            // content.Profile;
            // content.Profile.PlayerId;
            text = `${content.PlayFabId}: ${content.StatValue}`;
            /////////////////////////////////////////////
            let clonedNode = cc.instantiate( this.item_template.node );
            let label = clonedNode.getComponent(cc.Label);
            label.string = text;
            clonedNode.active = true;
            container.addChild( clonedNode );
        } // loop through the content array
    }, // updateScrollView

    //////////////////////////////////////////////////////////////////////////////////
    // User Interface handlers
    //////////////////////////////////////////////////////////////////////////////////
    ButtonPressed_Refresh :function () {
        cc.log( "ButtonPressed_Refresh" );
        cc.assert( null != this.identifier_editbox, "null identifier editbox" );
        cc.assert( null != this.score_editbox, "null score editbox" );

        let indentifier = this.identifier_editbox.string;
        let score = this.score_editbox.string;
        

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        let statisticName = indentifier;
        let startPosition = 0;
        let maxResultsCount = 100;


        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_Refresh.bind( this );

        // get the leaderboard data from server
        pfLeaderboard.GetLeaderboard(sessionToken, statisticName, startPosition, maxResultsCount, callbackFunc);
    }, // ButtonPressed_Refresh
    
    ButtonPressed_UploadScore :function () {
        cc.log( "ButtonPressed_UploadScore" );
        cc.assert( null != this.identifier_editbox, "null identifier editbox" );
        cc.assert( null != this.score_editbox, "null score editbox" );

        let indentifier = this.identifier_editbox.string;
        let score = this.score_editbox.string;
        

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        let statisticName = indentifier;
        let scores = [ {StatisticName: statisticName, Value: score} ];

        // upload the leaderboard score data to the server
        pfLeaderboard.UpdateLeaderboardScore(sessionToken, scores, 
            function (responseObj) {
            console.log( responseObj );
        });
    }, // ButtonPressed_UploadScore

    ButtonPressed_BackToMain :function () {
        let sceneIdentifier = "SceneList";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_BackToMain

    //////////////////////////////////////////////////////////////////////////////////
    // Network Request Response Callback
    //////////////////////////////////////////////////////////////////////////////////
    RequestCallback_Refresh :function (responseObj) {
        console.log( responseObj );

        let leaderboards = responseObj.data.Leaderboard;
        let version =  responseObj.data.Version;

        // update the scoreboard // 
        // validate the object 
        if ( undefined !== leaderboards || null !== leaderboards )
        {
            leaderboards;   
            // loop the array
            for (let i = 0; i < leaderboards.length; ++i)
            {
                let statistics = leaderboards[i];

                statistics.PlayFabId;
                statistics.Position;
                statistics.StatValue;
                
                statistics.Profile;
                statistics.Profile.PlayerId;

                console.log( `leaderboards[${i}]` );
                console.log( statistics );
            }
        }

        console.log( this );
        //
        this.updateScrollView( leaderboards );
    }, // RequestCallback_Refresh
});
