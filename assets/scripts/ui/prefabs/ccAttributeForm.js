
// data
const Contants = require("./Constants");

cc.Class({
    extends: cc.Component,

    properties: {
        adid_editbox : {
            type: cc.EditBox,
            default: null,
        }, // adid_editbox

        idfa_editbox : {
            type: cc.EditBox,
            default: null,
        }, // idfa_editbox
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////////
    // Method
    //////////////////////////////////////////////////////////////////////////////////////
    getTextByIdentifier :function (key) {
        cc.assert( undefined !== key && null !== key, "[key] parameter cannot be null or undefined" );

        if ( Contants.ui_keys.analytic_attribute_form_adid === key ) {
            cc.assert( null != this.adid_editbox, "null adid editbox" );
            return this.adid_editbox.string;
        } else if ( Contants.ui_keys.analytic_attribute_form_idfa === key ) {
            cc.assert( null != this.idfa_editbox, "null idfa editbox" );
            return this.idfa_editbox.string;
        }

        return null;
    }, // getTextByIdentifier
});
