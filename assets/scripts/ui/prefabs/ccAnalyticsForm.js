
// data
const Contants = require("./Constants");

cc.Class({
    extends: cc.Component,

    properties: {
        title_label : {
            type: cc.Label,
            default: null,
        }, // title_label

        identifier_editbox : {
            type: cc.EditBox,
            default: null,
        }, // identifier_editbox
        timestamp_editbox : {
            type: cc.EditBox,
            default: null,
        }, // timestamp_editbox
        body_editbox : {
            type: cc.EditBox,
            default: null,
        }, // body_editbox

        _state: 0,
        state : {
            get: function () { return this._state; }
        }
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////////
    // UI handler
    //////////////////////////////////////////////////////////////////////////////////////
    setTitleEvent: function() {
        cc.assert( null != this.title_label, "null title label" );

        this._state = Contants.enum_ui.ui_analytics_TitleEvent;
        this.title_label.string = "Write Title Event Form";
    }, // setCharacterEvent

    setPlayerEvent: function() {
        cc.assert( null != this.title_label, "null title label" );

        this._state = Contants.enum_ui.ui_analytics_PlayerEvent;
        this.title_label.string = "Write Player Event Form";
    }, // setCharacterEvent

    setCharacterEvent: function() {
        cc.assert( null != this.title_label, "null title label" );

        this._state = Contants.enum_ui.ui_analytics_CharacterEvent;
        this.title_label.string = "Write Character Event Form";
    }, // setCharacterEvent

    //////////////////////////////////////////////////////////////////////////////////////
    // Method
    //////////////////////////////////////////////////////////////////////////////////////
    getTextByIdentifier :function (key) {
        cc.assert( undefined !== key && null !== key, "[key] parameter cannot be null or undefined" );

        if ( Contants.ui_keys.analytic_data_form_identifier === key ) {
            cc.assert( null != this.identifier_editbox, "null identifier editbox" );
            return this.identifier_editbox.string;
        } else if ( Contants.ui_keys.analytic_data_form_timestamp === key ) {
            cc.assert( null != this.timestamp_editbox, "null timestamp editbox" );
            return this.timestamp_editbox.string;
        } else if ( Contants.ui_keys.analytic_data_form_body === key ) {
            cc.assert( null != this.body_editbox, "null body editbox" );
            return this.body_editbox.string;
        }

        return null;
    }, // getTextByIdentifier
});
