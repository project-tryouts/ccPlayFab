
// data
const Contants = require("./Constants");

cc.Class({
    extends: cc.Component,

    properties: {
        info_editbox : {
            type: cc.EditBox,
            default: null,
        }, // info_editbox

        native_toggle : {
          type: cc.Toggle,
          default: null,
      }, // native_toggle
        browser_toggle : {
          type: cc.Toggle,
          default: null,
      }, // browser_toggle
        mobile_toggle : {
          type: cc.Toggle,
          default: null,
      }, // mobile_toggle

        lang_label : {
          type: cc.Label,
          default: null,
      }, // lang_label

        os_label : {
          type: cc.Label,
          default: null,
      }, // os_label
        os_vr_label : {
          type: cc.Label,
          default: null,
      }, // os_vr_label
        os_main_vr_label : {
          type: cc.Label,
          default: null,
      }, // os_main_vr_label

        browser_type_label : {
          type: cc.Label,
          default: null,
      }, // browser_type_label
      browser_vr_label : {
          type: cc.Label,
          default: null,
      }, // browser_vr_label

    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        console.log( `cc.sys.isNative: ${cc.sys.isNative? "true":"false"}` );
        console.log( `cc.sys.isBrowser: ${cc.sys.isBrowser? "true":"false"}` );
        console.log( `cc.sys.isMobile: ${cc.sys.isMobile? "true":"false"}` );
  
        
        console.log( `cc.sys.language: ${cc.sys.language}` );
  
  
        console.log( `cc.sys.os: ${cc.sys.os}` );
        console.log( `cc.sys.osVersion: ${cc.sys.osVersion}` );
        console.log( `cc.sys.osMainVersion: ${cc.sys.osMainVersion}` );
  
  
        console.log( `cc.sys.browserType: ${cc.sys.browserType}` );
        console.log( `cc.sys.browserVersion: ${cc.sys.browserVersion}` );
  
        // return 1.0 if failure
        console.log( `cc.sys.getBatteryLevel: ${cc.sys.getBatteryLevel() !== 1.0? cc.sys.getBatteryLevel(): "failure"}` );
        
        console.log( `cc.sys.capabilities:` );
        console.log( cc.sys.capabilities );
        console.log( `cc.sys.localStorage:` );
        console.log( cc.sys.localStorage );
  
        {
          let logText = "cc.sys.os: ";
          switch ( cc.sys.os )
          {
            case cc.sys.OS_IOS:
              logText += "cc.sys.OS_IOS";
              break;
            case cc.sys.OS_ANDROID:
              logText += "cc.sys.OS_ANDROID";
              break;
            case cc.sys.OS_BLACKBERRY:
              logText += "cc.sys.OS_BLACKBERRY";
              break;
  
            case cc.sys.OS_WINDOWS:
              logText += "cc.sys.OS_WINDOWS";
              break;
            case cc.sys.OS_WP8:
              logText += "cc.sys.OS_WP8";
              break;
            case cc.sys.OS_WINRT:
              logText += "cc.sys.OS_WINRT";
              break;
            case cc.sys.OS_LINUX:
              logText += "cc.sys.OS_LINUX";
              break;
            case cc.sys.OS_OSX:
              logText += "cc.sys.OS_OSX";
              break;
  
  
            case cc.sys.OS_BADA:
              logText += "cc.sys.OS_BADA";
              break;
            case cc.sys.OS_MARMALADE:
              logText += "cc.sys.OS_MARMALADE";
              break;
            case cc.sys.OS_UNKNOWN:
              logText += "cc.sys.OS_UNKNOWN";
              break;
  
            default:
              logText += `${cc.sys.platform}`;
              break;
          }
          console.log( `${logText}` );
        }
        
        {
          let logText = "cc.sys.platform: ";
          switch ( cc.sys.platform )
          {
            case cc.sys.OS_IOS:
              logText += "cc.sys.OS_IOS";
              break;
            case cc.sys.OS_ANDROID:
              logText += "cc.sys.OS_ANDROID";
              break;
            case cc.sys.OS_BLACKBERRY:
              logText += "cc.sys.OS_BLACKBERRY";
              break;
  
            case cc.sys.OS_WINDOWS:
              logText += "cc.sys.OS_WINDOWS";
              break;
            case cc.sys.OS_WP8:
              logText += "cc.sys.OS_WP8";
              break;
            case cc.sys.OS_WINRT:
              logText += "cc.sys.OS_WINRT";
              break;
            case cc.sys.OS_LINUX:
              logText += "cc.sys.OS_LINUX";
              break;
            case cc.sys.OS_OSX:
              logText += "cc.sys.OS_OSX";
              break;
  
  
            case cc.sys.OS_BADA:
              logText += "cc.sys.OS_BADA";
              break;
            case cc.sys.OS_MARMALADE:
              logText += "cc.sys.OS_MARMALADE";
              break;
            case cc.sys.OS_UNKNOWN:
              logText += "cc.sys.OS_UNKNOWN";
              break;
  
  
  
  
            case cc.sys.DESKTOP_BROWSER:
              logText += "cc.sys.DESKTOP_BROWSER";
              break;
            case cc.sys.MOBILE_BROWSER:
              logText += "cc.sys.MOBILE_BROWSER";
              break;
            case cc.sys.WP8:
              logText += "cc.sys.WP8";
              break;
            case cc.sys.WINRT:
              logText += "cc.sys.WINRT";
              break;
            case cc.sys.TIZEN:
              logText += "cc.sys.TIZEN";
              break;
            case cc.sys.EMSCRIPTEN:
              logText += "cc.sys.EMSCRIPTEN";
              break;
            case cc.sys.NACL:
              logText += "cc.sys.NACL";
              break;
  
            case cc.sys.BLACKBERRY:
              logText += "cc.sys.BLACKBERRY";
              break;
            case cc.sys.IPAD:
              logText += "cc.sys.IPAD";
              break;
            case cc.sys.ANDROID:
              logText += "cc.sys.ANDROID";
              break;
            case cc.sys.MACOS:
              logText += "cc.sys.MACOS";
              break;
            case cc.sys.LINUX:
              logText += "cc.sys.LINUX";
              break;
            case cc.sys.WIN32:
              logText += "cc.sys.WIN32";
              break;
            case cc.sys.UNKNOWN:
              logText += "cc.sys.UNKNOWN";
              break;
  
            default:
              logText += `${cc.sys.platform}`;
              break;
          }
          console.log( `${logText}` );
        }
  
        // return cc.sys.NetworkType.LAN if failure
        switch ( cc.sys.getNetworkType() )
        {
          // Network is reachable via Wireless Wide Area Network
          case cc.sys.NetworkType.WWAN:
            console.log( `cc.sys.getNetworkType: NetworkType.WWAN` );
            break;
          // Network is reachable via WiFi or cable
          case cc.sys.NetworkType.LAN:
            console.log( `cc.sys.getNetworkType: NetworkType.LAN` );
            break;
          // Network is unreachable
          case cc.sys.NetworkType.NONE:
            console.log( `cc.sys.getNetworkType: NetworkType.NONE` );
            break;
        }
        
  
        
        console.log( cc.sys );

        cc.assert( null !== this.native_toggle, "null native toggle button" );
        if ( cc.sys.isNative ){
          this.native_toggle.check();
        } else {
          this.native_toggle.uncheck();
        }
        cc.assert( null !== this.browser_toggle, "null browser toggle button" );
        if ( cc.sys.isBrowser ){
          this.browser_toggle.check();
        } else {
          this.browser_toggle.uncheck();
        }
        cc.assert( null !== this.mobile_toggle, "null mobile toggle button" );
        if ( cc.sys.isMobile ){
          this.mobile_toggle.check();
        } else {
          this.mobile_toggle.uncheck();
        }
        
        cc.assert( null !== this.lang_label, "null language label" );
        this.lang_label.string = cc.sys.language;

        cc.assert( null !== this.os_label, "null operating system label" );
        this.os_label.string = cc.sys.os;
        cc.assert( null !== this.os_vr_label, "null operating system version label" );
        this.os_vr_label.string = cc.sys.osVersion;
        cc.assert( null !== this.os_main_vr_label, "null operating system main version label" );
        this.os_main_vr_label.string = cc.sys.osMainVersion;
  
        cc.assert( null !== this.browser_type_label, "null browser type label" );
        this.browser_type_label.string = cc.sys.browserType;
        cc.assert( null !== this.browser_vr_label, "null browser version label" );
        this.browser_vr_label.string = cc.sys.browserVersion;
        
    }, // start

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////////
    // Method
    //////////////////////////////////////////////////////////////////////////////////////
    getTextByIdentifier :function (key) {
        cc.assert( undefined !== key && null !== key, "[key] parameter cannot be null or undefined" );

        if ( Contants.ui_keys.analytic_device_form_info === key ) {
            cc.assert( null != this.info_editbox, "null info editbox" );


            let infoString = this.info_editbox.string;
            var bodyContent = { 
              Info: infoString

              //
              ,isNative:              cc.sys.isNative
              ,isBrowser:             cc.sys.isBrowser
              ,isMobile:              cc.sys.isMobile

              ,language:              cc.sys.language
              ,os:                    cc.sys.os
              ,osVersion:             cc.sys.osVersion
              ,osMainVersion:         cc.sys.osMainVersion

              ,browserType:           cc.sys.browserType
              ,browserVersion:        cc.sys.browserVersion

            };
            let jsonString = JSON.stringify(bodyContent);



            return jsonString;
        }

        return null;
    }, // getTextByIdentifier
});
