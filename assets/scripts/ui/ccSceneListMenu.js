
// data

// manager
const pfAccount = require("./playfab/pfAccount");

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        //if ( undefined === Global.Account || null === Global.Account) 
        if(true)
        {
            //
            let accountJSON = cc.sys.localStorage.Account;

            if ( undefined !== accountJSON || null !== accountJSON) 
            {
                let accountObject = null;
                try {
                    accountObject = JSON.parse(accountJSON);
                } catch (exception) {
                    console.log( exception );
                }
                
                if ( undefined !== accountObject && null !== accountObject) 
                {
                    console.log( accountObject );
                    // set the account data as the account object
                    Global.Account = accountObject;                
                }
                else
                {
                    console.log( "Account json unable to parse to object" );
                }
            }
            else
            {
                console.log( "Account json string not found in local storage" );
            }
        }
        else
        {
            console.log( "Local account exist in global variable" );
            console.log( Global.Account );
        }  
    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////
    // User Interface handlers
    ////////////////////////////////////////////////////////////////////////////////// 
    ButtonPressed_AccountScene :function () {
        let sceneIdentifier = "AccountScene";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_AccountScene

    ButtonPressed_LeaderboardScene :function () {
        let sceneIdentifier = "LeaderboardScene";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_LeaderboardScene

    ButtonPressed_GameConfigScene :function () {
        let sceneIdentifier = "GameConfigScene";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_GameConfigScene

    ButtonPressed_AnalyticsScene :function () {
        let sceneIdentifier = "AnalyticsScene";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_AnalyticsScene
});
