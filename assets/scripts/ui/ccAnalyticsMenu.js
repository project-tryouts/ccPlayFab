
// data
const Contants = require("./Constants");

// manager
const pfAnalytics = require("./playfab/pfAnalytics");

cc.Class({
    extends: cc.Component,

    properties: {
        form_ai: {
            default: null,
            type: cc.Node,
            tooltip: "Attribute Install Form",
        }, // form_ai

        form_rdi: {
            default: null,
            type: cc.Node,
            tooltip: "Report Device Info Form",
        }, // form_rdi

        form_wae: {
            default: null,
            type: cc.Node,
            tooltip: "Write {analytics} Event Form",
        }, // form_wae
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.assert( null != this.form_ai, "null Attribute Install Form" );
        cc.assert( null != this.form_rdi, "null Report Device Info Form" );
        cc.assert( null != this.form_wae, "null Write {analytics} Event Form" );
    }, // onLoad

    start () {

    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////
    // User Interface handlers
    //////////////////////////////////////////////////////////////////////////////////
    ButtonPressed_Submit :function (event, param) {
        cc.log( `ButtonPressed_Submit` );
        console.log( event );
        console.log( param );

        let eventName = param;

        if ( "AttributeInstall" === eventName ) {
            this.Logic_AttributeInstall();
        } else if ( "ReportDeviceInfo" === eventName ) {
            this.Logic_ReportDeviceInfo();
        } else if ( "writeEvent" === eventName ) {
            // get the state
            cc.assert( null != this.form_wae, "null Write {analytics} Event Form" );
            let formScript = this.form_wae.getComponent( "ccAnalyticsForm" );
            cc.assert( null != formScript, "null ccAnalyticsForm component" );
            let state = formScript.state;

            switch (state) {
                case Contants.enum_ui.ui_analytics_TitleEvent:
                    this.Logic_WriteTitleEvent();
                    break;
                case Contants.enum_ui.ui_analytics_PlayerEvent:
                    this.Logic_WritePlayerEvent();
                 break;
                case Contants.enum_ui.ui_analytics_CharacterEvent:
                    this.Logic_WriteCharacterEvent();
                    break;
                default:
                    break;
            } // switch
        } // "writeEvent" === eventName
    }, // ButtonPressed_Submit

    ButtonPressed_AttributeInstall :function () {
        cc.log( "ButtonPressed_AttributeInstall" );

        cc.assert( null != this.form_ai, "null Attribute Install Form" );
        cc.assert( null != this.form_rdi, "null Report Device Info Form" );
        cc.assert( null != this.form_wae, "null Write {analytics} Event Form" );

        // reset the ui state
        this.form_ai.active = false;
        this.form_rdi.active = false;
        this.form_wae.active = false;

        // allow the display of a ui element
        this.form_ai.active = true;
    }, // ButtonPressed_AttributeInstall

    ButtonPressed_ReportDeviceInfo :function () {
        cc.log( "ButtonPressed_ReportDeviceInfo" );

        cc.assert( null != this.form_ai, "null Attribute Install Form" );
        cc.assert( null != this.form_rdi, "null Report Device Info Form" );
        cc.assert( null != this.form_wae, "null Write {analytics} Event Form" );

        // reset the ui state
        this.form_ai.active = false;
        this.form_rdi.active = false;
        this.form_wae.active = false;

        // allow the display of a ui element
        this.form_rdi.active = true;
    }, // ButtonPressed_ReportDeviceInfo

    ButtonPressed_WriteTitleEvent :function () {
        cc.log( "ButtonPressed_WriteTitleEvent" );

        // reset the ui state
        this.form_ai.active = false;
        this.form_rdi.active = false;
        this.form_wae.active = false;

        // allow the display of a ui element
        this.form_wae.active = true;
        
        // setup the ui display
        let formScript = this.form_wae.getComponent( "ccAnalyticsForm" );
        cc.assert( null != formScript, "null ccAnalyticsForm component" );
        formScript.setTitleEvent();
    }, // ButtonPressed_WriteTitleEvent

    ButtonPressed_WritePlayerEvent :function () {
        cc.log( "ButtonPressed_WritePlayerEvent" );

        // reset the ui state
        this.form_ai.active = false;
        this.form_rdi.active = false;
        this.form_wae.active = false;

        // allow the display of a ui element
        this.form_wae.active = true;
        
        // setup the ui display
        let formScript = this.form_wae.getComponent( "ccAnalyticsForm" );
        cc.assert( null != formScript, "null ccAnalyticsForm component" );
        formScript.setPlayerEvent();
    }, // ButtonPressed_WritePlayerEvent

    ButtonPressed_WriteCharacterEvent :function () {
        cc.log( "ButtonPressed_WriteCharacterEvent" );

        // reset the ui state
        this.form_ai.active = false;
        this.form_rdi.active = false;
        this.form_wae.active = false;

        // allow the display of a ui element
        this.form_wae.active = true;

        // setup the ui display
        let formScript = this.form_wae.getComponent( "ccAnalyticsForm" );
        cc.assert( null != formScript, "null ccAnalyticsForm component" );
        formScript.setCharacterEvent();
    }, // ButtonPressed_WriteCharacterEvent

    ButtonPressed_BackToMain :function () {
        let sceneIdentifier = "SceneList";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_BackToMain

    //////////////////////////////////////////////////////////////////////////////////
    // Logic Handler
    //////////////////////////////////////////////////////////////////////////////////
    Logic_AttributeInstall :function () {
        console.log( "Logic_AttributeInstall" );
        
        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
                undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        
        
        let adid = ""; // advertising industry standard unique identifier 
        let idfa = ""; // The IdentifierForAdvertisers for iOS Devices

        var form = this.form_ai.getComponent("ccAttributeForm");
        cc.assert( undefined !== form && null !== form, "null component" );
        adid = form.getTextByIdentifier( Contants.ui_keys.analytic_attribute_form_adid );
        idfa = form.getTextByIdentifier( Contants.ui_keys.analytic_attribute_form_idfa );

cc.log(adid);
cc.log(idfa);

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_AttributeInstall.bind( this );

        // get the leaderboard data from server
        pfAnalytics.AttributeInstall(sessionToken, adid, idfa, callbackFunc);
    }, // Logic_AttributeInstall

    Logic_ReportDeviceInfo :function () {
        cc.log( "Logic_ReportDeviceInfo" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
                undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        
        
        let infoDictionary = {};

        var form = this.form_rdi.getComponent("ccDeviceForm");
        cc.assert( undefined !== form && null !== form, "null component" );
        infoDictionary = form.getTextByIdentifier( Contants.ui_keys.analytic_device_form_info );

cc.log(infoDictionary);


        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_ReportDeviceInfo.bind( this );

        // get the leaderboard data from server
        pfAnalytics.ReportDeviceInfo(sessionToken, infoDictionary, callbackFunc);
    }, // Logic_ReportDeviceInfo

    Logic_WriteTitleEvent :function () {
        cc.log( "Logic_WriteTitleEvent" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
                undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        
        
        let eventName = "";
        let timestamp = "";
        let body = {};

        var form = this.form_wae.getComponent("ccAnalyticsForm");
        cc.assert( undefined !== form && null !== form, "null component" );
        eventName = form.getTextByIdentifier( Contants.ui_keys.analytic_data_form_identifier );
        timestamp = form.getTextByIdentifier( Contants.ui_keys.analytic_data_form_timestamp );
        body = form.getTextByIdentifier( Contants.ui_keys.analytic_data_form_body );

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_WriteTitleEvent.bind( this );

        // get the leaderboard data from server
        pfAnalytics.WriteTitleEvent(sessionToken, eventName, timestamp, body, callbackFunc);
    }, // Logic_WriteTitleEvent

    Logic_WritePlayerEvent :function () {
        cc.log( "Logic_WritePlayerEvent" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        
        
        let eventName = "";
        let timestamp = "";
        let body = {};


        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_WritePlayerEvent.bind( this );

        // get the leaderboard data from server
        pfAnalytics.WritePlayerEvent(sessionToken, eventName, timestamp, body, callbackFunc);
    }, // Logic_WritePlayerEvent

    Logic_WriteCharacterEvent :function () {
        cc.log( "Logic_WriteCharacterEvent" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }
        
        
        let eventName = "";
        let timestamp = "";
        let body = {};


        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_WriteCharacterEvent.bind( this );

        // get the leaderboard data from server
        pfAnalytics.WriteCharacterEvent(sessionToken, eventName, timestamp, body, callbackFunc);
    }, // Logic_WriteCharacterEvent

    //////////////////////////////////////////////////////////////////////////////////
    // Network Request Response Callback
    //////////////////////////////////////////////////////////////////////////////////
    RequestCallback_AttributeInstall :function (responseObj) {
        console.log( responseObj );
    }, // RequestCallback_AttributeInstall

    RequestCallback_ReportDeviceInfo :function (responseObj) {
        console.log( responseObj );
    }, // RequestCallback_ReportDeviceInfo

    RequestCallback_WriteTitleEvent :function (responseObj) {
        console.log( responseObj );
    }, // RequestCallback_WriteTitleEvent

    RequestCallback_WritePlayerEvent :function (responseObj) {
        console.log( responseObj );
    }, // RequestCallback_WritePlayerEvent

    RequestCallback_WriteCharacterEvent :function (responseObj) {
        console.log( responseObj );
    }, // RequestCallback_WriteCharacterEvent
});
