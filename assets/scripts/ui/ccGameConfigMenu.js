
// manager
const pfGameConfig = require("./playfab/pfGameConfig");

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        console.log( Global.Account );
    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    // User Interface handlers
    //////////////////////////////////////////////////////////////////////////////////
    ButtonPressed_GetServerTime :function () {
        cc.log( "ButtonPressed_GetServerTime" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetServerTime.bind( this );

        // load the game news from the server
        pfGameConfig.GetTime(sessionToken, callbackFunc);
    }, // ButtonPressed_GetServerTime

    ButtonPressed_GetNews :function () {
        cc.log( "ButtonPressed_GetNews" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetNews.bind( this );

        let count = 10;

        // load the game news from the server
        pfGameConfig.GetTitleNews(sessionToken, count, callbackFunc);
    }, // ButtonPressed_GetNews

    ButtonPressed_GetGameData :function () {
        cc.log( "ButtonPressed_GetGameData" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetGameData.bind( this );

        let keys = null;

        // load the game data from the server
        pfGameConfig.GetTitleData(sessionToken, keys, callbackFunc);
    }, // ButtonPressed_GetGameData

    
    ButtonPressed_GetPublisherData :function () {
        cc.log( "ButtonPressed_GetPublisherData" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetPublisherData.bind( this );

        let keys = [];

        // load the game data from the server
        pfGameConfig.GetPublisherData(sessionToken, keys, callbackFunc);
    }, // ButtonPressed_GetPublisherData

    ButtonPressed_GetStoreItems :function () {
        cc.log( "ButtonPressed_GetStoreItems" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetStoreItems.bind( this );

        let identifier = "shop-a1";
        let version = "";

        // load the game data from the server
        pfGameConfig.GetStoreItems(sessionToken, identifier, version, callbackFunc);
    }, // ButtonPressed_GetStoreItems

    ButtonPressed_GetCatalogItems :function () {
        cc.log( "ButtonPressed_GetCatalogItems" );

        // TODO:
        let sessionToken = null;
        if ( undefined !== Global.Account || null !== Global.Account ||
             undefined !== Global.Account.SessionTicket || null !== Global.Account.SessionTicket)
        {
            sessionToken = Global.Account.SessionTicket;
        }

        // define the callback function, bind the function object with this object instance as caller
        let callbackFunc = this.RequestCallback_GetCatalogItems.bind( this );

        let version = "";

        // load the game data from the server
        pfGameConfig.GetCatalogItems(sessionToken, version, callbackFunc);
    }, // ButtonPressed_GetCatalogItems

    ButtonPressed_BackToMain :function () {
        let sceneIdentifier = "SceneList";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_BackToMain

    //////////////////////////////////////////////////////////////////////////////////
    // Network Request Response Callback
    //////////////////////////////////////////////////////////////////////////////////
    RequestCallback_GetServerTime :function (responseObj) {
        console.log( responseObj );

        let serverTimeObject = responseObj.data;
        console.log( serverTimeObject );
        let serverTime = serverTimeObject.Time;
        console.log( serverTime );

    }, // RequestCallback_GetServerTime

    RequestCallback_GetNews :function (responseObj) {
        console.log( responseObj );

        let news = responseObj.data.News;

        // validate the object 
        if ( undefined !== news || null !== news ) {
            console.log( news );

            // for (let i = 0; i < news.length; ++i) {
            //     let newsObject = news[i];
            //     newsObject.Timestamp;
            //     newsObject.Title;
            //     newsObject.Body;
            // }
        } // validate the object 

    }, // RequestCallback_GetNews

    RequestCallback_GetGameData :function (responseObj) {
        console.log( responseObj );

        let datas = responseObj.data.Data;

        // validate the object 
        if ( undefined !== datas || null !== datas ) {
            console.log( datas );
        } // validate the object 

    }, // RequestCallback_GetGameData

    RequestCallback_GetPublisherData :function (responseObj) {
        console.log( responseObj );

        let datas = responseObj.data.Data;

        // validate the object 
        if ( undefined !== datas || null !== datas ) {
            console.log( datas );
        } // validate the object 

    }, // RequestCallback_GetPublisherData

    RequestCallback_GetStoreItems :function (responseObj) {
        console.log( responseObj );

        let source = responseObj.data.Source;
        let version = responseObj.data.CatalogVersion;
        let identifier = responseObj.data.StoreId;
        
        let marketingDatas = responseObj.data.MarketingData;
        // validate the object 
        if ( undefined !== marketingDatas || null !== marketingDatas ) {
            console.log( marketingDatas );

            // for (let i = 0; i < marketingDatas.length; ++i) {
            //     let marketingData = marketingDatas[i];
            //     marketingData.DisplayName;
            //     marketingData.Description;
            // }
        } // validate the object 

        let products = responseObj.data.Store;
        // validate the object 
        if ( undefined !== products || null !== products ) {
            console.log( products );

            // for (let i = 0; i < products.length; ++i) {
            //     let product = products[i];
            //     product.ItemId;
            //     product.VirtualCurrencyPrices; // object
            //     product.RealCurrencyPrices; // object
            // }
        } // validate the object 

    }, // RequestCallback_GetStoreItems

    RequestCallback_GetCatalogItems :function (responseObj) {
        console.log( responseObj );

        let products = responseObj.data.Catalog;
        // validate the object 
        if ( undefined !== products || null !== products ) {
            console.log( products );

            // for (let i = 0; i < products.length; ++i) {
            //     let product = products[i];
            //     product.ItemId;
            //     product.ItemClass;
            //     product.CatalogVersion;
            //     product.DisplayName;
            //     product.Description;

            //     product.CanBecomeCharacter;
            //     product.IsStackable;
            //     product.IsTradable;
            //     product.IsLimitedEdition;
            //     product.InitialLimitedEditionCount;

            //     product.Container; // object
            //     product.Container.VirtualCurrencyContents; // object
            //     product.Bundle; // object
            //     product.Bundle.BundledItems; // array of string
            //     product.Bundle.BundledResultTables; // array of string
            //     product.Bundle.BundledVirtualCurrencies; // object
            //     product.Consumable; // object
            //     product.VirtualCurrencyPrices; // object
            //     product.RealCurrencyPrices; // object
            // }
        } // validate the object 

    }, // RequestCallback_GetCatalogItems
});
