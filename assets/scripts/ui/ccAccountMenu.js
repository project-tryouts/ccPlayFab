
// data

// manager
const pfAccount = require("./playfab/pfAccount");

cc.Class({
    extends: cc.Component,

    properties: {
        username_editbox : {
            type: cc.EditBox,
            default: null,
        }, // username_editbox
        
        email_editbox : {
            type: cc.EditBox,
            default: null,
        }, // email_editbox
        
        password_editbox : {
            type: cc.EditBox,
            default: null,
        }, // password_editbox
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},

    //////////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////////////
    checkCredentialsValidity :function (username, email, password) {
        // check if the required variables checks out
        if (null != username && 0 < username.length &&
            //null != email && 0 < email.length && // set it optional, FOR NOW
            null != password && 0 < password.length) 
        {
            cc.log( `username: ${username}` );
            cc.log( `email: ${email}` );
            cc.log( `password: ${password}` );
            return true;
        }
        else
        {
            if ( undefined == username || null == username || 0 >= username.length )
            {
                cc.log( `invalid username` );
            }
            // set it optional, FOR NOW
            // else if ( undefined == email || null == email || 0 >= email.length )
            // {
            //     cc.log( `invalid email` );
            // }
            else if ( undefined == password || null == password || 0 >= password.length )
            {
                cc.log( `invalid password` );
            }
        }

        return false;
    }, // checkCredentialsValidity

    //////////////////////////////////////////////////////////////////////////////////
    // User Interface handlers
    //////////////////////////////////////////////////////////////////////////////////    
    ButtonPressed_Register :function () {
        cc.log( "ButtonPressed_Register" );
        cc.assert( null != this.username_editbox, "null username editbox" );
        cc.assert( null != this.email_editbox, "null email editbox" );
        cc.assert( null != this.password_editbox, "null password editbox" );

        let username = this.username_editbox.string;
        let email = this.email_editbox.string;
        let password = this.password_editbox.string;


        let valid = this.checkCredentialsValidity(username, email, password);
        if ( valid )
        {
            // create game account
            pfAccount.RegisterPlayFabAccount(username, email, password, function (responseObj) {
                console.log( responseObj );

                // set the response data as the account object
                Global.Account = responseObj.data;
                
                let responseObjJSON = JSON.stringify( { response: responseObj } );
                cc.sys.localStorage.AccountCreate = responseObjJSON;

                {
                    let accountObjJSON = JSON.stringify( responseObj.data );
                    cc.sys.localStorage.Account = accountObjJSON;
                  }
            });
        }
    }, // ButtonPressed_Register

    ButtonPressed_Login :function () {
        cc.log( "ButtonPressed_Login" );
        cc.assert( null != this.username_editbox, "null username editbox" );
        cc.assert( null != this.email_editbox, "null email editbox" );
        cc.assert( null != this.password_editbox, "null password editbox" );

        let username = this.username_editbox.string;
        let email = this.email_editbox.string;
        let password = this.password_editbox.string;

        let valid = this.checkCredentialsValidity(username, email, password);
        if ( valid )
        {
            // login game account
            pfAccount.LoginToPlayFab(username, password, function (responseObj) {
                console.log( responseObj );

                // set the response data as the account object
                Global.Account = responseObj.data;

                let responseObjJSON = JSON.stringify( { response: responseObj } );
                cc.sys.localStorage.AccountLogin = responseObjJSON;

                {
                    let accountObjJSON = JSON.stringify( responseObj.data );
                    cc.sys.localStorage.Account = accountObjJSON;
                  }
            });
        }
    }, // ButtonPressed_Login

    ButtonPressed_BackToMain :function () {
        let sceneIdentifier = "SceneList";

        // load the scene with the indentifier
        cc.director.loadScene(`${sceneIdentifier}`, function() {
            cc.log( `onLaunched callback: scene [${sceneIdentifier}]` );
        });
    }, // ButtonPressed_BackToMain
});
