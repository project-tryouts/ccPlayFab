// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

const pfAccount = require("./playfab/pfAccount");

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
      ;
      console.log( `cc.sys.isNative: ${cc.sys.isNative? "true":"false"}` );
      console.log( `cc.sys.isBrowser: ${cc.sys.isBrowser? "true":"false"}` );
      console.log( `cc.sys.isMobile: ${cc.sys.isMobile? "true":"false"}` );

      
      console.log( `cc.sys.language: ${cc.sys.language}` );


      console.log( `cc.sys.os: ${cc.sys.os}` );
      console.log( `cc.sys.osVersion: ${cc.sys.osVersion}` );
      console.log( `cc.sys.osMainVersion: ${cc.sys.osMainVersion}` );


      console.log( `cc.sys.browserType: ${cc.sys.browserType}` );
      console.log( `cc.sys.browserVersion: ${cc.sys.browserVersion}` );

      // return 1.0 if failure
      console.log( `cc.sys.getBatteryLevel: ${cc.sys.getBatteryLevel() !== 1.0? cc.sys.getBatteryLevel(): "failure"}` );
      
      console.log( `cc.sys.capabilities:` );
      console.log( cc.sys.capabilities );
      console.log( `cc.sys.localStorage:` );
      console.log( cc.sys.localStorage );

      {
        let logText = "cc.sys.os: ";
        switch ( cc.sys.os )
        {
          case cc.sys.OS_IOS:
            logText += "cc.sys.OS_IOS";
            break;
          case cc.sys.OS_ANDROID:
            logText += "cc.sys.OS_ANDROID";
            break;
          case cc.sys.OS_BLACKBERRY:
            logText += "cc.sys.OS_BLACKBERRY";
            break;

          case cc.sys.OS_WINDOWS:
            logText += "cc.sys.OS_WINDOWS";
            break;
          case cc.sys.OS_WP8:
            logText += "cc.sys.OS_WP8";
            break;
          case cc.sys.OS_WINRT:
            logText += "cc.sys.OS_WINRT";
            break;
          case cc.sys.OS_LINUX:
            logText += "cc.sys.OS_LINUX";
            break;
          case cc.sys.OS_OSX:
            logText += "cc.sys.OS_OSX";
            break;


          case cc.sys.OS_BADA:
            logText += "cc.sys.OS_BADA";
            break;
          case cc.sys.OS_MARMALADE:
            logText += "cc.sys.OS_MARMALADE";
            break;
          case cc.sys.OS_UNKNOWN:
            logText += "cc.sys.OS_UNKNOWN";
            break;

          default:
            logText += `${cc.sys.platform}`;
            break;
        }
        console.log( `${logText}` );
      }
      
      {
        let logText = "cc.sys.platform: ";
        switch ( cc.sys.platform )
        {
          case cc.sys.OS_IOS:
            logText += "cc.sys.OS_IOS";
            break;
          case cc.sys.OS_ANDROID:
            logText += "cc.sys.OS_ANDROID";
            break;
          case cc.sys.OS_BLACKBERRY:
            logText += "cc.sys.OS_BLACKBERRY";
            break;

          case cc.sys.OS_WINDOWS:
            logText += "cc.sys.OS_WINDOWS";
            break;
          case cc.sys.OS_WP8:
            logText += "cc.sys.OS_WP8";
            break;
          case cc.sys.OS_WINRT:
            logText += "cc.sys.OS_WINRT";
            break;
          case cc.sys.OS_LINUX:
            logText += "cc.sys.OS_LINUX";
            break;
          case cc.sys.OS_OSX:
            logText += "cc.sys.OS_OSX";
            break;


          case cc.sys.OS_BADA:
            logText += "cc.sys.OS_BADA";
            break;
          case cc.sys.OS_MARMALADE:
            logText += "cc.sys.OS_MARMALADE";
            break;
          case cc.sys.OS_UNKNOWN:
            logText += "cc.sys.OS_UNKNOWN";
            break;




          case cc.sys.DESKTOP_BROWSER:
            logText += "cc.sys.DESKTOP_BROWSER";
            break;
          case cc.sys.MOBILE_BROWSER:
            logText += "cc.sys.MOBILE_BROWSER";
            break;
          case cc.sys.WP8:
            logText += "cc.sys.WP8";
            break;
          case cc.sys.WINRT:
            logText += "cc.sys.WINRT";
            break;
          case cc.sys.TIZEN:
            logText += "cc.sys.TIZEN";
            break;
          case cc.sys.EMSCRIPTEN:
            logText += "cc.sys.EMSCRIPTEN";
            break;
          case cc.sys.NACL:
            logText += "cc.sys.NACL";
            break;

          case cc.sys.BLACKBERRY:
            logText += "cc.sys.BLACKBERRY";
            break;
          case cc.sys.IPAD:
            logText += "cc.sys.IPAD";
            break;
          case cc.sys.ANDROID:
            logText += "cc.sys.ANDROID";
            break;
          case cc.sys.MACOS:
            logText += "cc.sys.MACOS";
            break;
          case cc.sys.LINUX:
            logText += "cc.sys.LINUX";
            break;
          case cc.sys.WIN32:
            logText += "cc.sys.WIN32";
            break;
          case cc.sys.UNKNOWN:
            logText += "cc.sys.UNKNOWN";
            break;

          default:
            logText += `${cc.sys.platform}`;
            break;
        }
        console.log( `${logText}` );
      }

      // return cc.sys.NetworkType.LAN if failure
      switch ( cc.sys.getNetworkType() )
      {
        // Network is reachable via Wireless Wide Area Network
        case cc.sys.NetworkType.WWAN:
          console.log( `cc.sys.getNetworkType: NetworkType.WWAN` );
          break;
        // Network is reachable via WiFi or cable
        case cc.sys.NetworkType.LAN:
          console.log( `cc.sys.getNetworkType: NetworkType.LAN` );
          break;
        // Network is unreachable
        case cc.sys.NetworkType.NONE:
          console.log( `cc.sys.getNetworkType: NetworkType.NONE` );
          break;
      }
      

      
      console.log( cc.sys );
    },

    // update (dt) {},


    getDeviceInfo: function()
    {
      let info = {
        os                    : cc.sys.os,
        os_version            : cc.sys.osVersion,
        os_main_version       : cc.sys.osMainVersion,

        native                : cc.sys.isNative,
        mobile                : cc.sys.isMobile,
        platform              : cc.sys.platform,
        language              : cc.sys.language,

        browser               : cc.sys.isBrowser,
        browser_type          : cc.sys.browserType,
        browser_version       : cc.sys.browserVersion,
      };
      console.log( info );
      return info;
    },
    
    //////////////////////////////////////////////////////////////////////
    // 
    //////////////////////////////////////////////////////////////////////
    GetTitlePublicKey: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      // login the account
      pfAccount.GetTitlePublicKey(function (responseObj) {
        console.log( "GetTitlePublicKey response" );
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////
        
        let responseObjJSON = JSON.stringify(responseObj);
        cc.sys.localStorage.GetTitlePublicKey = { response: responseObjJSON };
      });
    }, // GetTitlePublicKey

    LoginWithCustomID: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      // login the account
      pfAccount.LoginWithCustomID("tester", true, function (responseObj) {
        console.log( "LoginWithCustomID response" );
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////

        let responseObjJSON = JSON.stringify( { response: responseObj } );
        cc.sys.localStorage.LoginWithCustomID = responseObjJSON;
        console.log( responseObjJSON );
        console.log( cc.sys.localStorage );
      });
    }, // LoginWithCustomID

    AccountLogin: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      // login the account
      pfAccount.LoginToPlayFab("theuser", "thepassword", function (responseObj) {
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////

        let responseObjJSON = JSON.stringify(responseObj);
        cc.sys.localStorage.AccountLogin = { response: responseObjJSON };
      });
    }, // AccountLogin

    AccountCreate: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      // login the account
      pfAccount.RegisterPlayFabAccount("theuser", "me@here.com", "thepassword", function (responseObj) {
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////

        let responseObjJSON = JSON.stringify(responseObj);
        cc.sys.localStorage.AccountCreate = { response: responseObjJSON };
      });
    }, // AccountLogin

    AccountLogin: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      // login the account
      pfAccount.LoginToPlayFab("theuser", "thepassword", function (responseObj) {
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////

        let responseObjJSON = JSON.stringify(responseObj);
        cc.sys.localStorage.AccountLogin.response = responseObjJSON;
      });
    }, // AccountLogin

    AccountGetInfo: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);

      console.log("Global.playfabSessionTicket");
      console.log(Global.playfabSessionTicket);
      console.log("Global.playfabUserId");
      console.log(Global.playfabUserId);

      // get the login session details
      let playfabSessionTicket = Global.playfabSessionTicket;
      // get the user's identifier
      let playfabUserId = Global.playfabUserId;

      if(undefined === playfabSessionTicket || undefined === playfabUserId ||
         null === playfabSessionTicket || null === playfabUserId) {
         // cannot access [this], because this function is not bind this object
         if(null !== label) {
           let labelString = "";
           if(undefined === playfabSessionTicket || undefined === playfabSessionTicket)
             labelString += "null [Global.playfabSessionTicket] variable\n";
           if(undefined === playfabUserId || undefined === playfabUserId)
             labelString += "null [playfabUserId] variable\n";

           // update the label
           label.string = labelString;
         } //////////////////////////////////////////////////////////////////////
      } else {


        // get the user's data
        pfAccount.GetAccountInfo(playfabSessionTicket, playfabUserId, function (responseObj) {
          console.log("responseObj");
          console.log(responseObj);

          // cannot access [this], because this function is not bind to the object
          if(null !== label) {
            let responseString = JSON.stringify(responseObj, null, 2);
            label.string = responseString;
          } //////////////////////////////////////////////////////////////////////

          Global.playfabUserId;
        });
      }
    }, // AccountGetInfo


    //////////////////////////////////////////////////////////////////////
    ReportDeviceInfo: function () {
      // get the local variable, as the delegate function will not be able to access this object
      let labelNode = cc.find('Canvas/console-display');
      let label = labelNode.getComponent(cc.Label);


      let token = null;
      let infoDictionary = null;

      // login the account
      pfAccount.ReportDeviceInfo(token, infoDictionary, function (responseObj) {
        console.log( responseObj );

        // cannot access [this], because this function is not bind this object
        if(null !== label) {
          let responseString = JSON.stringify(responseObj, null, 2);
          label.string = responseString;
        } //////////////////////////////////////////////////////////////////////

        let responseObjJSON = JSON.stringify(responseObj);
        cc.sys.localStorage.ReportDeviceInfo.response = responseObjJSON;
      });
    }, // ReportDeviceInfo
});
