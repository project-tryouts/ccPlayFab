/**
 * XMLHttpRequest
 * Reference:
 * https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
 * https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState

 * XMLHttpRequest.readyState
 * 0	UNSENT	Client has been created. open() not called yet.
 * 1	OPENED	open() has been called.
 * 2	HEADERS_RECEIVED	send() has been called, and headers and status are available.
 * 3	LOADING	Downloading; responseText holds partial data.
 * 4	DONE	The operation is complete.

 */

/**
 * @param   url           string              URL for the HTTP request
 * @param   method        string              HTTP request method
 * @param   headers       object              HTTP request, only top level keys and values.
 * @param   body          string              HTTP request content body
 * @param   callback      function            HTTP response callback function
 */
export function SendHTTPRequest (url, method, headers, body, callback = undefined) {
  // crates the HTTP request object
  var xhr = new XMLHttpRequest();
  // set the callback function for the HTTP network object
  xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
          // deseralize the JSON string to an object
          let response = xhr.response;
          let responseObj = JSON.parse(response);

          // pass the response object to the callback function
          if(undefined !== callback) {
            callback(responseObj);
          }
      }
  };
  // initializes the request
  xhr.open(method, url, true);

  // NOTE: MUST TRIGGER after open the request object
  // Send the proper header information along with the request
  //xhr.setRequestHeader("Content-type", "application/json");
  if (undefined !== headers && null !== headers) {
    let objKeys = Object.keys(headers);
    if (undefined !== objKeys && null !== objKeys) {
      for (let i = 0; i < objKeys.length; i++) {
        let objKey = objKeys[i];
        let objValue = headers[objKey];

        // only allow string to be set in the header
        if (typeof objValue === 'string' || objValue instanceof String) {
          // set the HTTP reques headers
          xhr.setRequestHeader(objKey, objValue);
        } // value type check
      } // loop through all the keys in the header object
    } // check if header object is valid
  } // check if header object is valid


  // send the data to the URL
  xhr.send(body);
} // SendHTTPRequest
