
const pfConstants = require("pfConstants");
const NetworkSystem = require("../common/NetworkSystem");


/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/GetLeaderboard
 * 
 * @param {String}    sessionToken 
 * @param {String}    StatisticName                       Statistic used to rank players for this leaderboard
 * @param {Number}    StartPosition                       Position in the leaderboard to start this listing (defaults to the first entry).
 * @param {Number}    MaxResultsCount                     Maximum number of entries to retrieve. Default 10, maximum 100
 * @param {Function}  callback 
 */
export function GetLeaderboard (sessionToken, statisticName, startPosition, maxResultsCount, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetLeaderboard`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { StatisticName: statisticName, StartPosition: startPosition, MaxResultsCount: maxResultsCount };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetLeaderboard


/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/UpdatePlayerStatistics
 * 
 * Object (StatisticName)
 * - StatisticName: string,
 * - Value: number,
 * - Version: number // optional
 * 
 * @param {String}                  sessionToken 
 * @param {[StatisticName]}         statistics                        Statistics to be updated with the provided values
 * @param {Function}                callback 
 */
export function UpdateLeaderboardScore (sessionToken, statistics, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/UpdatePlayerStatistics`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { Statistics : statistics };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // UpdateLeaderboardScore
