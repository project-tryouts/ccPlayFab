// addtional reference to add scripts to CocosCreator
// http://docs.cocos.com/creator/manual/en/
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.8/en/

// reference to XMLHttpRequest:
// https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest


// PlayFab js sdk
// https://api.playfab.com/docs/getting-started/javascript-getting-started
// note:
// Tested but not able to run script, unable to find script.

// PlayFab NodeJS sdk
// https://api.playfab.com/platforms#nodejs
// note:
// Tested but not able to compile, unable to include into the build process

// PlayFab HTTPS api
// https://api.playfab.com/documentation


// Unique identifier for the title,
// found in the Settings > Game Properties section
// of the PlayFab developer site when a title has been selected.
const pfApplicationIdentifier = "db1e";

// Secret Keys for the title,
// found in the Settings > Secret Keys section
// of the PlayFab developer site when a title has been selected.
const pfApplicationSecretKey = "3BU91UQJJNTNA3M8X8U6WC369OCR3O1W7GGPX8DKR43R5D5KYH";

// Unique identifier for the title,
// found in the Settings > Game Properties section
// of the PlayFab developer site when a title has been selected.
const pfHostURL = "https://db1e.playfabapi.com/";

module.exports = {
  ApplicationIdentifier: pfApplicationIdentifier,
  SecretKey: pfApplicationSecretKey,
  HostURL: pfHostURL,
};
