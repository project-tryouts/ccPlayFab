
const pfConstants = require("pfConstants");
const NetworkSystem = require("../common/NetworkSystem");

/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/AttributeInstall
 * https://en.wikipedia.org/wiki/Ad-ID
 * 
 * @param {String}    sessionToken 
 * @param {String}    Adid                      The adid for this device, advertising industry standard unique identifier 
 * @param {String}    Idfa                      The IdentifierForAdvertisers for iOS Devices
 * @param {Function}  callback 
 */
export function AttributeInstall (sessionToken, Adid, Idfa, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/AttributeInstall`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  var bodyContent = { Idfa: Idfa, Idfa: Idfa };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // AttributeInstall





/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/ReportDeviceInfo
 * 
 * @param {String}      sessionToken 
 * @param {Object}      infoDictionary 
 * @param {Function}    callback 
 */
export function ReportDeviceInfo (sessionToken, infoDictionary, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/ReportDeviceInfo`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  var bodyContent = { Info: infoDictionary };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // ReportDeviceInfo


/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/WriteTitleEvent
 * 
 * @param {String}    sessionToken 
 * @param {String}    EventName                      Custom event properties. Each property consists of a name (string) and a value (JSON object).
 * @param {String}    Timestamp                      The name of the event, within the namespace scoped to the title. The naming convention is up to the caller, but it commonly follows the subject_verb_object pattern (e.g. player_logged_in).
 * @param {Object}    Body                           The time (in UTC) associated with this event. The value dafaults to the current time.


 * @param {Function}  callback 
 */
export function WriteTitleEvent (sessionToken, eventName, timestamp, body, callback = undefined) {
    var url = `${pfConstants.HostURL}Client/WriteTitleEvent`;
    var method = "POST";
    var headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    var bodyContent = { EventName: eventName, Timestamp: timestamp, Body: body };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // WriteTitleEvent



/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/WritePlayerEvent
 * 
 * @param {String}    sessionToken 
 * @param {String}    EventName                      Custom event properties. Each property consists of a name (string) and a value (JSON object).
 * @param {String}    Timestamp                      The name of the event, within the namespace scoped to the title. The naming convention is up to the caller, but it commonly follows the subject_verb_object pattern (e.g. player_logged_in).
 * @param {Object}    Body                           The time (in UTC) associated with this event. The value dafaults to the current time.
 * @param {Function}  callback 
 */
export function WritePlayerEvent (sessionToken, eventName, timestamp, body, callback = undefined) {
    var url = `${pfConstants.HostURL}Client/WritePlayerEvent`;
    var method = "POST";
    var headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    var bodyContent = { EventName: eventName, Timestamp: timestamp, Body: body };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // WritePlayerEvent



/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/WriteCharacterEvent
 * 
 * @param {String}    sessionToken 
 * @param {String}    EventName                      Custom event properties. Each property consists of a name (string) and a value (JSON object).
 * @param {String}    CharacterId                    Unique PlayFab assigned ID for a specific character owned by a user
 * @param {String}    Timestamp                      The name of the event, within the namespace scoped to the title. The naming convention is up to the caller, but it commonly follows the subject_verb_object pattern (e.g. player_logged_in).
 * @param {Object}    Body                           The time (in UTC) associated with this event. The value dafaults to the current time.
 * @param {Function}  callback 
 */
export function WriteCharacterEvent (sessionToken, eventName, characterId , timestamp, body, callback = undefined) {
    var url = `${pfConstants.HostURL}Client/WriteCharacterEvent`;
    var method = "POST";
    var headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    var bodyContent = { EventName: eventName, CharacterId: characterId , Timestamp: timestamp, Body: body };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // WriteCharacterEvent


