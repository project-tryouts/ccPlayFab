
const pfConstants = require("pfConstants");
const NetworkSystem = require("../common/NetworkSystem");


/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetTime
 * 
 * @param {String}    sessionToken 
 * @param {Function}  callback 
 */
export function GetTime (sessionToken, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetTime`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, null, callback);
  } // GetTime

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetTitleNews
 * 
 * @param {String}    sessionToken 
 * @param {Number}    count                       Limits the results to the last n entries. Defaults to 10 if not set.
 * @param {Function}  callback 
 */
export function GetTitleNews (sessionToken, count, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetTitleNews`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { Count: count };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetTitleNews


/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetTitleData
 * 
 * @param {String}          sessionToken 
 * @param {[String]}        Keys                       Specific keys to search for in the title data (leave null to get all keys)
 * @param {Function}        callback 
 */
export function GetTitleData (sessionToken, keys, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetTitleData`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { Keys: keys };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetTitleData




/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetPublisherData
 * 
 * @param {String}          sessionToken 
 * @param {[String]}        Keys                       array of keys to get back data from the Publisher data blob, set by the admin tools
 * @param {Function}        callback 
 */
export function GetPublisherData (sessionToken, keys, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetPublisherData`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { Keys: keys };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetPublisherData


  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetStoreItems
 * 
 * @param {String}          sessionToken 
 * @param {String}          identifier                      Unqiue identifier for the store which is being requested.
 * @param {String}          version                         Catalog version to store items from. Use default catalog version if null
 * @param {Function}        callback 
 */
export function GetStoreItems (sessionToken, identifier, version, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetStoreItems`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { StoreId : identifier, CatalogVersion : version };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetStoreItems


/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetCatalogItems
 * 
 * @param {String}          sessionToken 
 * @param {String}          version                         Which catalog is being requested. If null, uses the default catalog.
 * @param {Function}        callback 
 */
export function GetCatalogItems (sessionToken, version, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/GetCatalogItems`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    let bodyContent = { CatalogVersion : version };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // GetCatalogItems
