import { WriteCharacterEvent } from "./pfAnalytics";

// definition
const pfConstants = require("pfConstants");

// manager
const NetworkSystem = require("../common/NetworkSystem");

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/AddUserVirtualCurrency
 * 
 * @param {String}          sessionToken 
 * @param {String}          currencyID                      Name of the virtual currency which is to be incremented.
 * @param {Number}          amount                          Amount to be added to the user balance of the specified virtual currency.
 * @param {Function}        callback 
 */
export function AddUserVirtualCurrency (sessionToken, currencyID, amount, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/AddUserVirtualCurrency`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    /////////////////////////////////////////////////////////////////////////
    let bodyContent = { 
        VirtualCurrency: currencyID, 
        Amount: amount 
    };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // AddUserVirtualCurrency

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/SubtractUserVirtualCurrenc
 * 
 * @param {String}          sessionToken 
 * @param {String}          currencyID                      Name of the virtual currency which is to be decremented.
 * @param {Number}          amount                          Amount to be subtracted from the user balance of the specified virtual currency.
 * @param {Function}        callback 
 */
export function SubtractUserVirtualCurrency (sessionToken, currencyID, amount, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/SubtractUserVirtualCurrency`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    /////////////////////////////////////////////////////////////////////////
    let bodyContent = { 
        VirtualCurrency: currencyID, 
        Amount: amount 
    };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // SubtractUserVirtualCurrency

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/PurchaseItem
 * 
 * @param {String}          sessionToken 
 * @param {String}          identifier                  Unique identifier of the item to purchase.
 * @param {String}          currency                    Virtual currency to use to purchase the item..
 * @param {Number}          cost                        Price the client expects to pay for the item (in case a new catalog or store was uploaded, with new prices).
 * @param {String}          catalog                     Catalog version for the items to be purchased (defaults to most recent version.
 * @param {String}          store                       Store to buy this item through. If not set, prices default to those in the catalog.
 * @param {String}          character                   Unique PlayFab assigned ID for a specific character owned by a user.
 * @param {Function}        callback 
 */
export function PurchaseItem (sessionToken, identifier, currency, cost, catalog, store, character, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/PurchaseItem`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    /////////////////////////////////////////////////////////////////////////
    let bodyContent = { 
        ItemId: identifier, 
        VirtualCurrency: currency, 
        Price: cost, 
        // optional
        CatalogVersion: catalog, 
        CharacterId: character, 
        StoreId: store 
    };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // PurchaseItem

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/ConsumeItem
 * 
 * @param {String}          sessionToken 
 * @param {String}          instanceID                      Unique PlayFab assigned ID for a specific character owned by a user
 * @param {Number}          count                           Number of uses to consume from the item.
 * @param {String}          character                       Unique instance identifier of the item to be consumed.
 * @param {Function}        callback 
 */
export function ConsumeItem (sessionToken, instanceID, count, character, callback = undefined) {
    let url = `${pfConstants.HostURL}Client/ConsumeItem`;
    let method = "POST";
    let headers = {
      "Content-type": "application/json",
      "X-Authentication": sessionToken,
    };
    /////////////////////////////////////////////////////////////////////////
    let bodyContent = { 
        ItemInstanceId : instanceID, 
        ConsumeCount : count,
        // optional
        CharacterId: character
    };
    let jsonString = JSON.stringify(bodyContent);
    /////////////////////////////////////////////////////////////////////////
    NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
  } // ConsumeItem

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/RedeemCoupon
 * 
 * @param {String}          sessionToken 
 * @param {String}          code                            Generated coupon code to redeem
 * @param {String}          version                         Catalog version of the coupon. If null, uses the default catalog.
 * @param {String}          character                       Optional identifier for the Character that should receive the item. If null, item is added to the player.
 * @param {Function}        callback 
 */
export function RedeemCoupon (sessionToken, code, version, character, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/RedeemCoupon`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      CouponCode : code, 
      // optional
      CatalogVersion: version,
      CharacterId : character
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // RedeemCoupon

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetUserInventory
 * 
 * @param {String}          sessionToken 
 * @param {Function}        callback 
 */
export function GetUserInventory (sessionToken, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/GetUserInventory`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, null, callback);
} // GetUserInventory

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetCharacterInventory
 * 
 * @param {String}          sessionToken 
 * @param {String}          identifier                            Used to limit results to only those from a specific catalog version
 * @param {String}          version                               Unique PlayFab assigned ID for a specific character owned by a user.
 * @param {Function}        callback 
 */
export function GetCharacterInventory (sessionToken, identifier, version, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/GetCharacterInventory`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      CharacterId  : identifier, 
      // optional
      CatalogVersion: version
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // GetCharacterInventory

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/GetPurchase
 * 
 * @param {String}          sessionToken 
 * @param {String}          OrderId                               Purchase order identifier
 * @param {Function}        callback 
 */
export function GetPurchase (sessionToken, identifier, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/GetPurchase`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      OrderId: identifier, 
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // GetPurchase

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/StartPurchase
 * 
 * @param {String}          sessionToken 
 * @param {[object]}        items                                 Array of items to purchase
 * @param {String}          version                               Catalog version for the items to be purchased. Defaults to most recent catalog.
 * @param {String}          store                                 Store through which to purchase items. If not set, prices will be pulled from the catalog itself
 * @param {Function}        callback 
 */
export function StartPurchase (sessionToken, items, version, store, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/StartPurchase`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      Items : items, 
      // optional
      CatalogVersion: version,
      StoreId: store
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // StartPurchase

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/PayForPurchase
 * 
 * @param {String}          sessionToken 
 * @param {String}          currency                              Currency to use to fund the purchase
 * @param {String}          orderId                               Purchase order identifier returned from StartPurchase
 * @param {String}          providerName                          Payment provider to use to fund the purchase.
 * @param {String}          transactionId                         Payment provider transaction identifier. Required for Facebook Payments.
 * @param {Function}        callback 
 */
export function PayForPurchase (sessionToken, currency, orderId, providerName, transactionId, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/PayForPurchase`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      Currency : items, 
      OrderId  : items, 
      ProviderName  : items, 
      // optional
      ProviderTransactionId: version,
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // PayForPurchase

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/ConfirmPurchase
 * 
 * @param {String}          sessionToken 
 * @param {String}          orderId                               Purchase order identifier returned from StartPurchase
 * @param {Function}        callback 
 */
export function ConfirmPurchase (sessionToken, orderId, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/ConfirmPurchase`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      OrderId  : items, 
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // ConfirmPurchase

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/UnlockContainerInstance
 * 
 * @param {String}          sessionToken 
 * @param {String}          containerId                             ItemInstanceId of the container to unlock
 * @param {String}          version                                 Specifies the catalog version that should be used to determine container contents. If unspecified, uses catalog associated with the item instance
 * @param {String}          character                               Unique PlayFab assigned ID for a specific character owned by a user
 * @param {String}          itemId                                  ItemInstanceId of the key that will be consumed by unlocking this container. If the container requires a key, this parameter is required
 * @param {Function}        callback 
 */
export function UnlockContainerInstance (sessionToken, containerId, version, character, itemId, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/UnlockContainerInstance`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      ContainerItemInstanceId: containerId, 
      // optional
      CatalogVersion: version,
      CharacterId: character,
      KeyItemInstanceId: itemId
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // UnlockContainerInstance

/**
 * Reference:
 * https://api.playfab.com/documentation/Client/method/UnlockContainerItem
 * 
 * @param {String}          sessionToken 
 * @param {String}          containerId                             ItemInstanceId of the container to unlock
 * @param {String}          version                                 Specifies the catalog version that should be used to determine container contents. If unspecified, uses catalog associated with the item instance
 * @param {String}          character                               Unique PlayFab assigned ID for a specific character owned by a user
 * @param {Function}        callback 
 */
export function UnlockContainerItem (sessionToken, containerId, version, character, callback = undefined) {
  let url = `${pfConstants.HostURL}Client/UnlockContainerItem`;
  let method = "POST";
  let headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  /////////////////////////////////////////////////////////////////////////
  let bodyContent = { 
      ContainerItemId : containerId, 
      // optional
      CatalogVersion: version,
      CharacterId: character
  };
  let jsonString = JSON.stringify(bodyContent);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, jsonString, callback);
} // UnlockContainerItem
