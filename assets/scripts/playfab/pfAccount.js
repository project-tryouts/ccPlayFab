/**
 * XMLHttpRequest
 * Reference:
 * https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
 * https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState

 * XMLHttpRequest.readyState
 * 0	UNSENT	Client has been created. open() not called yet.
 * 1	OPENED	open() has been called.
 * 2	HEADERS_RECEIVED	send() has been called, and headers and status are available.
 * 3	LOADING	Downloading; responseText holds partial data.
 * 4	DONE	The operation is complete.

 */

const pfConstants = require("pfConstants");
const NetworkSystem = require("../common/NetworkSystem");

// https://api.playfab.com/documentation/client/method/LoginWithPlayFab
var pfLoginAccountRequest = {
  endpoint: "Client/LoginWithPlayFab",
  method: "POST",
  body: [
    "TitleId",
    "Username",
    "Password",
  ],
};

// https://api.playfab.com/documentation/client/method/GetAccountInfo#request-properties
var pfGetAccountInfoRequest = {
  endpoint: "Client/GetAccountInfo",
  method: "POST",
  body: [
    "PlayFabId",
    "Username",
  ],
};

export function RegisterPlayFabAccount (username, email, password, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/RegisterPlayFabUser`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
  };
  var content = {
    Username: username,
    Email: email,
    Password: password,
    TitleId: pfConstants.ApplicationIdentifier,
  };
  var bodyContent = JSON.stringify(content);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // RegisterPlayFabAccount

export function LoginToPlayFab (username, password, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/LoginWithPlayFab`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
  };
  var content = {
    Username: username,
    Password: password,
    TitleId: pfConstants.ApplicationIdentifier,
  };
  var bodyContent = JSON.stringify(content);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // LoginToPlayFab

export function GetAccountInfo (sessionTicket, identifier, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/GetAccountInfo`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionTicket,
  };
  var content = {
    PlayFabId: identifier,
  };
  var bodyContent = JSON.stringify(content);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // GetAccountInfo


export function LoginWithCustomID (identifier, createAccountFlag = false, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/LoginWithCustomID`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
  };
  var content = {
    CustomId: identifier,
    CreateAccount: createAccountFlag,
    TitleId: pfConstants.ApplicationIdentifier,
  };
  var bodyContent = JSON.stringify(content);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // RegisterPlayFabAccount



export function GetTitlePublicKey (callback = undefined) {
  var url = `${pfConstants.HostURL}Client/GetTitlePublicKey`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
  };
  var content = {
    TitleSharedSecret: pfConstants.SecretKey,
    TitleId: pfConstants.ApplicationIdentifier,
  };
  var bodyContent = JSON.stringify(content);
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // LoginToPlayFab










/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/GetPlayerStatistics
 * 
 * @param {String}    sessionToken 
 * @param {String}    StatisticNames                  statistics to return (current version will be returned for each)
 * @param {String}    StatisticNameVersions           statistics to return, if StatisticNames is not set (only statistics which have a version matching that provided will be returned)
 * @param {Function}  callback 
 */
export function GetPlayerStatistics (sessionToken, statisticNames, statisticNameVersions = null, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/GetPlayerStatistics`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  var bodyContent = { StatisticNames: statisticNames, StatisticNameVersions: statisticNameVersions };
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // GetPlayerStatistics






/**
 * Reference:
 * https://api.playfab.com/documentation/client/method/GetPlayerStatisticVersions
 * 
 * @param {String}    sessionToken 
 * @param {String}    StatisticName                  unique name of the statistic
 * @param {Function}  callback 
 */
export function GetPlayerStatisticVersions (sessionToken, statisticName, callback = undefined) {
  var url = `${pfConstants.HostURL}Client/GetPlayerStatisticVersions`;
  var method = "POST";
  var headers = {
    "Content-type": "application/json",
    "X-Authentication": sessionToken,
  };
  var bodyContent = { StatisticName: statisticName };
  /////////////////////////////////////////////////////////////////////////
  NetworkSystem.SendHTTPRequest(url, method, headers, bodyContent, callback);
} // GetPlayerStatisticVersions

